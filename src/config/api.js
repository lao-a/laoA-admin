import services from '../utils/services';

const authPrefix = '/auth_api';
const prefix = '/api';

const sysApis = {
  getUser: {
    type: 'GET',
    url: `${authPrefix}/user`,
  },
  signin: {
    type: 'GET',
    url: `${prefix}/signin`,
  },
  signout: {
    type: 'GET',
    url: `${authPrefix}/signout`,
  },
  getActivityList: {
    type: 'GET',
    url: `${authPrefix}/activity`,
  },
  getActivity: {
    type: 'GET',
    url: `${authPrefix}/activity/:actId`,
  },
  updateActivity: {
    type: 'PUT',
    url: `${authPrefix}/activity/:actId`,
  },
  saveActivity: {
    type: 'POST',
    url: `${authPrefix}/activity`,
  },
  newCardNum: {
    type: 'POST',
    url: `${authPrefix}/cardnum`,
  },
  getMember: {
    type: 'GET',
    url: `${authPrefix}/member/:memId`,
  },
  getMemberList: {
    type: 'GET',
    url: `${authPrefix}/member`,
  },
  saveMember: {
    type: 'POST',
    url: `${authPrefix}/member`,
  },
  updateMember: {
    type: 'PUT',
    url: `${authPrefix}/member/:memId`,
  },
  getActivityTypes: {
    type: 'GET',
    url: `${authPrefix}/dict/activity/type`,
  },
  getMemberTypes: {
    type: 'GET',
    url: `${prefix}/dict/member/type`,
  },
  getMemberLevels: {
    type: 'GET',
    url: `${prefix}/dict/member/level`,
  },
  uploadImage: {
    type: 'POST',
    url: `${authPrefix}/image`,
  },
};

// 用数组替换url中的参数部分
const replaceUrl = (source, replacement) => {
  if (!replacement || !replacement.length) return source;

  let i = -1;
  return source.replace(/:[^/]+/g, () => {
    i += 1;
    return replacement[i];
  });
};

// () => () => {}
export const interact = (api, ...args) => {
  const { type, url } = sysApis[api];
  const target = replaceUrl(url, args);

  return (data) => {
    return services[type](target, data);
  };
};

export default sysApis;
