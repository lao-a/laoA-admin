import React from 'react';
import { connect } from 'dva';
import { Card, Button, Radio, Tag, Row, Col } from 'antd';
import { numToFixed } from './utils';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import DescriptionList from '../../../components/DescriptionList';
import Detail from './Detail';
import PointsList from './Points';
import CashList from './Cash';
import LivenessList from './Liveness';
import InviteList from './Invite';

const { Description } = DescriptionList;

class MemberBody extends React.Component {
  state = {
    tabKey: 'detail',
  };

  tabList = [
    {
      key: 'detail',
      tab: '详细信息',
    },
    {
      key: 'coupon',
      tab: '拥有卡券',
    },
    {
      key: 'invite',
      tab: '邀请记录',
    },
    {
      key: 'points',
      tab: '积分流水',
    },
    {
      key: 'cash',
      tab: '现金流水',
    },
    {
      key: 'liveness',
      tab: '活跃度流水',
    },
  ];

  strongStyle = {
    fontSize: '1.2em',
    fontWeight: '600',
  };

  renderHeadTitle = () => {
    const { detail = {} } = this.props;

    return detail.MemId ? (
      <>
        <span style={{display: 'inline-block', marginRight: '16px'}}>
          {`${detail.Name} - (№: ${detail.CardNum})`}
        </span>
      </>
    ) : '会员详情';
  }

  renderHeadAction = () => {
    return (
      <React.Fragment>
        <Button.Group>
          <Button>充值</Button>
          <Button>提现</Button>
        </Button.Group>
        <Button type="primary">审核</Button>
      </React.Fragment>
    );
  }

  renderHeadContent = () => {
    const {
      detail={},
      memberTypes = [],
      memberLevels = [],
    } = this.props;

    return detail.MemId ? (
      <DescriptionList size="small" col="2">
        <Description term="会员类型">
          <span>
            {(memberTypes.filter(({TypeId}) => TypeId === detail.MemType)[0] || {}).TypeName || ''}
          </span>
        </Description>
        <Description term="会员等级">
          <span>
            {(memberLevels.filter(({LevelId}) => LevelId === detail.LevelId)[0] || {}).Name || ''}
          </span>
        </Description>
        <Description term="联系方式">{detail.Phone}</Description>
        <Description term="联系地址">{detail.Address}</Description>
      </DescriptionList>
    ) : undefined;
  }

  renderHeadExtra = () => {
    const { detail={} } = this.props;
    
    return (
      <React.Fragment>
        <Row>
          <Col xs={24} sm={12}>
            <div>状态</div>
            <div style={this.strongStyle}>
              <Tag color={detail.Status === 2 ? '#666' : (detail.Status === 1 ? '#87d068' : '#f50')}>
                {detail.Status === 2 ? '注销' : (detail.Status === 1 ? '正常' : '待审核')}
              </Tag>
            </div>
          </Col>
          <Col xs={24} sm={12}>
            <div>活跃度</div>
            <div style={this.strongStyle}>{detail.Liveness}</div>
          </Col>
        </Row>
        <Row style={{ marginTop: '16px' }}>
          <Col xs={24} sm={12}>
            <div>账户余额</div>
            <div style={this.strongStyle}>{`￥ ${numToFixed(detail.CashLeft, 2)}`}</div>
          </Col>
          <Col xs={24} sm={12}>
            <div>积分</div>
            <div style={this.strongStyle}>{detail.Points}</div>
          </Col>
        </Row>
      </React.Fragment>
    );
  }

  renderCardTab = () => {
    const {
      account = {},
      inviteList = [],
    } = this.props.detail || {};

    const {
      pointsList = [],
      cashList = [],
      livenessList = [],
    } = account;

    switch(this.state.tabKey) {
      case 'detail':
        return <Detail {...this.props} />;
      case 'coupon':
        return <div>卡券记录, 马上上线...</div>;
      case 'invite':
        return <InviteList data={inviteList} />;
      case 'points':
        return <PointsList data={pointsList} />;
      case 'cash':
        return <CashList data={cashList} />;
      case 'liveness':
        return <LivenessList data={livenessList} />;
      default:
        return <Detail {...this.props} />;
    }
  };

  render() {
    const newMode = true;
    const detail = this.props.detail || {};

    return (
      <PageHeaderLayout
        title={this.renderHeadTitle()}
        logo={detail.Avatar}
        action={this.renderHeadAction()}
        content={this.renderHeadContent()}
        extraContent={this.renderHeadExtra()}
      >
        <Card
          bordered={false}
          tabList={this.tabList}
          onTabChange={tabKey => this.setState({ tabKey })}
        >
          {this.renderCardTab()}
        </Card>
      </PageHeaderLayout>
    );
  }
}

export default connect(s => s.member)(MemberBody);
