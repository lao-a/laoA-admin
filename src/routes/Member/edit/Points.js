import React, { PureComponent } from 'react';
import { Table, Tag, Badge } from 'antd';
import { getFormatDate } from './utils';

const { Column } = Table;

export default class extends PureComponent {

  render() {
    return (
      <div>
        <Table dataSource={this.props.data}>
          <Column
            title="时间"
            key="date"
            render={x => getFormatDate(x.CreateDate)}
          />
          <Column
            title="积分"
            key="points"
            render={(raw) => {
              return raw.InOut === 1
                ? <Tag color="#389e0d">{`+ ${raw.Points}`}</Tag>
                : <Tag color="#ff0000">{`- ${raw.Points}`}</Tag>
            }}
          />
          <Column
            title="描述"
            key="desc"
            dataIndex="SrcDesc"
          />
        </Table>
      </div>
    );
  }
}
