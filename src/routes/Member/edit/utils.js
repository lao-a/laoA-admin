import moment from 'moment';

export function getFormatDate(dt) {
  if (!dt || dt.indexOf('0001') === 0) return '';

  return moment(dt).format('YYYY-MM-DD HH:mm');
};

export function numToFixed(fl, num) {
  return new Number(fl).toFixed(num);
}
