import React, { PureComponent } from 'react';
import { Table } from 'antd';
import { getFormatDate } from './utils';

const { Column } = Table;

export default class extends PureComponent {

  render() {
    return (
      <div>
        <Table dataSource={this.props.data}>
          <Column
            title="时间"
            key="date"
            render={x => getFormatDate(x.CreateDate)}
          />
          <Column
            title="活跃度"
            key="liveness"
            dataIndex="Liveness"
          />
          <Column
            title="描述"
            key="desc"
            dataIndex="Desc"
          />
        </Table>
      </div>
    );
  }
}
