import React, { PureComponent } from 'react';
import { Form } from 'antd';

export default class WarpItem extends PureComponent {
  render() {
    return (
      <Form.Item
        labelCol={{
          xs: { span: 24 },
          sm: { span: 6 },
        }}
        wrapperCol={{
          xs: { span: 24 },
          sm: { span: 14 },
        }}
        label={this.props.label}
      >
        {this.props.children}
      </Form.Item>
    );
  }
}
