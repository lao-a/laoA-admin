import React, { PureComponent } from 'react';
import { Table, Tag, Badge } from 'antd';
import { getFormatDate, numToFixed } from './utils';

const { Column } = Table;

export default class extends PureComponent {

  render() {
    return (
      <div>
        <Table dataSource={this.props.data}>
          <Column
            title="时间"
            key="date"
            render={x => getFormatDate(x.CreateDate)}
          />
          <Column
            title="金额"
            key="charges"
            render={(raw) => {
              return raw.InOut === 1
                ? <Tag color="#389e0d">{`+ ${numToFixed(raw.Charges, 2)}`}</Tag>
                : <Tag color="#ff0000">{`- ${numToFixed(raw.Charges, 2)}`}</Tag>
            }}
          />
          <Column
            title="描述"
            key="desc"
            dataIndex="Desc"
          />
        </Table>
      </div>
    );
  }
}
