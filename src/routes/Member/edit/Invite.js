import React, { PureComponent } from 'react';
import { Table, Tag, Badge } from 'antd';
import { getFormatDate } from './utils';

const { Column } = Table;

export default class extends PureComponent {

  render() {
    return (
      <div>
        <Table dataSource={this.props.data}>
          <Column
            title="时间"
            key="date"
            render={x => getFormatDate(x.InviteDate)}
          />
          <Column
            title="被邀请人"
            key="invitee_name"
            dataIndex="InviteeName"
          />
          <Column
            title="描述"
            key="desc"
            dataIndex="InviteDesc"
          />
        </Table>
      </div>
    );
  }
}
