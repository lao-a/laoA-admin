import React, { Component } from 'react';
import moment from 'moment';
import { connect } from 'dva';
import { routerRedux, Link } from 'dva/router';
import { Form, Card, List, Icon, Avatar, Button, Input } from 'antd';

import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import StandardFormRow from '../../components/StandardFormRow';
import TagSelect from '../../components/TagSelect';
import styles from './SearchList.less';

const FormItem = Form.Item;

const pageSize = 5;

@Form.create()
@connect(s => s.activities)
export default class ActivityList extends Component {
  componentDidMount() {
    this.fetchMore();
  }

  setOwner = () => {
    const { form } = this.props;
    form.setFieldsValue({
      owner: ['wzj'],
    });
  }

  fetchMore = () => {
    this.props.dispatch({
      type: 'list/fetch',
      payload: {
        count: pageSize,
      },
    });
  }

  handleTabChange = (key) => {
    const { dispatch } = this.props;
    switch (key) {
      case 'docs':
        dispatch(routerRedux.push('/list/search'));
        break;
      case 'app':
        dispatch(routerRedux.push('/list/filter-card-list'));
        break;
      case 'project':
        dispatch(routerRedux.push('/list/cover-card-list'));
        break;
      default:
        break;
    }
  }

  render() {
    const loading = false;
    const { form, list } = this.props;
    const { getFieldDecorator } = form;

    const IconText = ({ type, text }) => (
      <span>
        <Icon type={type} style={{ marginRight: 8 }} />
        {text}
      </span>
    );

    const ListContent = ({ data: { Special, CreateDate, UserName, TypeId, JoinNum } }) => (
      <div className={styles.listContent}>
        <div className={styles.description} dangerouslySetInnerHTML={{ __html: Special }} />
        <div className={styles.extra}>
          <Avatar icon="user" size="small" />{UserName} 发布
          <em>{((this.props.types || []).filter(x => x.TypeId === TypeId)[0] || {}).TypeName}</em>
          <em>{moment(CreateDate).format('YYYY-MM-DD hh:mm')}</em>
          <em><Icon type="team" size="small" /> {JoinNum}</em>
        </div>
      </div>
    );

    const pageHeaderContent = (
      <div style={{ textAlign: 'center' }}>
        <Input.Search
          placeholder="请输入"
          enterButton="搜索"
          size="large"
          onSearch={this.handleFormSubmit}
          style={{ width: 522 }}
        />
      </div>
    );

    const loadMore = list.length > 0 ? (
      <div style={{ textAlign: 'center', marginTop: 16 }}>
        <Button onClick={this.fetchMore} style={{ paddingLeft: 48, paddingRight: 48 }}>
          {loading ? <span><Icon type="loading" /> 加载中...</span> : '加载更多'}
        </Button>
      </div>
    ) : null;

    return (
      <PageHeaderLayout
        title="搜索列表"
        content={pageHeaderContent}
        onTabChange={this.handleTabChange}
      >
        <div>
          <Card bordered={false}>
            <Form layout="inline">
              <StandardFormRow title="所属分类" block style={{ paddingBottom: 11 }}>
                <FormItem>
                  {getFieldDecorator('category')(
                    <TagSelect onChange={this.handleFormSubmit} expandable>
                      {
                        (this.props.types || []).map(({ TypeId, TypeName }) => {
                          return (<TagSelect.Option key={`${TypeId}`} value={`${TypeId}`}>{TypeName}</TagSelect.Option>);
                        })
                      }
                    </TagSelect>
                  )}
                </FormItem>
              </StandardFormRow>
            </Form>
          </Card>
          <Card
            style={{ marginTop: 24 }}
            bordered={false}
            bodyStyle={{ padding: '8px 32px 32px 32px' }}
          >
            <List
              size="large"
              loading={list.length === 0 ? loading : false}
              rowKey="id"
              itemLayout="vertical"
              loadMore={loadMore}
              dataSource={list}
              renderItem={item => (
                <List.Item
                  key={item.ActivityId}
                  extra={<div className={styles.listItemExtra} />}
                >
                  <List.Item.Meta
                    title={(
                      <Link className={styles.listItemMetaTitle} to={`/activity/edit/${item.ActivityId}`}>{item.Title}</Link>
                    )}
                  />
                  <ListContent data={item} />
                </List.Item>
              )}
            />
          </Card>
        </div>
      </PageHeaderLayout>
    );
  }
}
