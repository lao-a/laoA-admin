import React from 'react';
import { connect } from 'dva';
import { Card } from 'antd';
import Result from '../../components/Result';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';

const ResultPage = (props) => {
  return (
    <PageHeaderLayout>
      <Card bordered={false}>
        <Result
          style={{ marginTop: 48, marginBottom: 16 }}
          {...props.result}
        />
      </Card>
    </PageHeaderLayout>
  );
};

export default connect(s => s.result)(ResultPage);
