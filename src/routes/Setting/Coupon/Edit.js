import React, { Component } from 'react';
import { connect } from 'dva';
import { routerRedux, Link } from 'dva/router';
import {
  Form, Input, InputNumber, DatePicker, Button, Card, Radio, Select, Switch,
} from 'antd';
import FormBody from 'xrc-form';
import moment from 'moment';
import PageHeaderLayout from '../../../layouts/PageHeaderLayout';
import WrapItem from '../../../components/StandardFormRow/SimpleItem';
import Uploader from '../../../components/Uploader';
import { interact } from '../../../config/api';
import { statusOk } from '../../../utils/httpcode';

const handleUpload = (file) => {
  return new Promise((res, rej) => {
    interact('uploadImage')({ UpImg: file }).then(({ code, message }) => {
      if (code !== statusOk) {
        rej(message);
      }

      res(message.Url);
    });
  });
};

const { RangePicker } = DatePicker;

@connect(s => s.coupon)
@Form.create()
export default class Coupon extends Component {
  render() {
    const { detail = {}, form } = this.props;

    const fields = [
      {
        label: '卡券名称',
        name: 'Name',
        element: <Input placeholder="填写卡券名称" />,
        options: {
          initialValue: detail.Name,
          rules: [{
            required: true,
            message: '请填写卡券名称',
          }],
        },
      },
      {
        label: '卡券图',
        name: 'Thumb',
        element: <Uploader onUpload={handleUpload} />,
        options: {
          initialValue: detail.Thumb || '',
        },
      },
      {
        label: '减免金额',
        name: 'Charge',
        element: <InputNumber
          style={{ width: '100%' }}
          placeholder="填写减免金额"
          formatter={v => `￥ ${v}`}
          parser={v => v.replace(/￥\s?/ig, '')}
        />,
        options: {
          initialValue: detail.Charge,
          rules: [{
            required: true,
            message: '请填写减免金额',
          }],
        },
      },
      {
        label: '有效期类型',
        name: 'ExpireType',
        element: (
          <Radio.Group>
            <Radio.Button value="by-range">指定范围</Radio.Button>
            <Radio.Button value="by-time">指定天数</Radio.Button>
          </Radio.Group>
        ),
        options: {
          initialValue: detail.ExpireType,
          rules: [{
            required: true,
            message: '请选择有效期类型',
          }],
        },
      },
      {
        label: '有效期范围',
        name: 'ExpireRange',
        element: <RangePicker style={{ width: '100%' }} placeholder={['开始日期', '结束日期']} />,
        options: {
          initialValue: [moment(detail.ExpireStart), moment(detail.ExpireEnd)],
        },
      },
      {
        label: '有效期(天)',
        name: 'ExpireTo',
        element: <InputNumber
          style={{ width: '100%' }}
          placeholder="填写有效期限"
        />,
        options: {
          initialValue: detail.ExpireTo,
        },
      },
      {
        label: '限制活动',
        name: 'ActivityType',
        element: (
          <Select style={{ width: '100%' }}>
            <Select.Option key="%">不限制</Select.Option>
            {
              (this.props.actTypes || []).map(({ TypeId, TypeName }) => {
                return (
                  <Select.Option key={`${TypeId}`}>{TypeName}</Select.Option>
                );
              })
            }
          </Select>
        ),
        options: {
          initialValue: detail.ActivityType,
        },
      },
      {
        label: '发放方式',
        name: 'DistributeType',
        element: (
          <Radio.Group>
            <Radio.Button value="nolimit">任意</Radio.Button>
            <Radio.Button value="new-one">新用户</Radio.Button>
          </Radio.Group>
        ),
        options: {
          initialValue: detail.DistributeType,
          rules: [{
            required: true,
            message: '请选择发放方式',
          }],
        },
      },
    ];

    const handleSubmit = (e) => {
      e.preventDefault();
      form.validateFields((err, values) => {
        if (err) {
          return false;
        }

        const [ ExpireStart, ExpireEnd ] = values.ExpireRange || [];

      });
    };

    return (
      <PageHeaderLayout title="卡券维护">
        <Card bordered={false}>
          <Form onSubmit={handleSubmit} style={{ marginTop: 8, maxWidth: '800px' }}>
            <FormBody items={fields} wrapItem={WrapItem} form={form} />
          </Form>
        </Card>
      </PageHeaderLayout>
    );
  }
}
