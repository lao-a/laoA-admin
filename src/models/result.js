
export default {
  namespace: 'result',
  state: {
    default: {
      type: '',
      title: '',
      extra: undefined,
      actions: undefined,
    },
    result: {
      type: '',
      title: '',
      extra: undefined,
      actions: undefined,
    },
  },

  reducers: {
    updateResult(state, { payload }) {
      const result = { ...state.result, ...state.default, ...payload };
      return { ...state, result };
    },
  },
};
