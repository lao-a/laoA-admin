import modelExtend from 'dva-model-extend';
import pathToRegexp from 'path-to-regexp';
import { message } from 'antd';
import { interact } from '../config/api';
import { statusOk } from '../utils/httpcode';
import { searchParams } from '../utils/utils';
import base from './base';

const PAGE_LIST = 'index_list';

export default modelExtend(base, {
  namespace: 'member',
  state: {
    memberTypes: [],
    memberLevels: [],
    newCardNum: '',
    defaultMemType: {},
    defaultCardType: {},
  },

  effects: {
    *getMemTypes(_, { call, put }) {
      const { code, message: result } = yield call(interact('getMemberTypes'));
      if (code === statusOk) {
        yield put({ type: 'syncState', payload: { memberTypes: result.MemberTypes } });
      } else {
        throw new Error(result);
      }
    },
    *getMemLevels(_, { call, put }) {
      const { code, message: result } = yield call(interact('getMemberLevels'));
      if (code === statusOk) {
        yield put({ type: 'syncState', payload: { memberLevels: result.MemberLevels } });
      } else {
        throw new Error(result);
      }
    },
    *getCardNum({ payload }, { call, put }) {
      const { code, message: result } = yield call(interact('newCardNum'), { cardtype: payload || '' });

      if (code !== statusOk) {
        throw new Error(result);
      }

      yield put({ type: 'syncState', payload: { newCardNum: result.CardNum } });
    },
    *loadList({ payload }, { call, put }) {
      // payload 为url search
      // 暂时只支持 page
      const page = payload.page || 0;
      const { code, message: result } = yield call(interact('getMemberList'), { page });

      if (code === statusOk) {
        yield put({ type: 'syncState', payload: { list: result.Members } });
        yield put({ type: 'syncPageNavi', payload: { id: PAGE_LIST, current: page } });
      } else {
        throw new Error(result);
      }

      // 获取分类
      yield put({ type: 'getMemTypes' });
      yield put({ type: 'getMemLevels' });
    },
    *loadEdit({ payload }, { call, put }) {
      if (!payload) {
        yield put({ type: 'getCardNum' });
      } else {
        const { code, message: result } = yield call(interact('getMember', [payload]));

        if (code === statusOk) {
          if (!result.Member.MemId) {
            throw new Error('没有找到对应的会员');
          }

          const detail = {
            ...result.Member,
            account: {
              pointsList: result.PointsList || [],
              cashList: result.CashList || [],
              livenessList: result.LivenessList || [],
            },
            inviteList: result.InviteList || [],
          }

          yield put({ type: 'syncState', payload: { detail } });
        } else {
          throw new Error(result);
        }
      }

      yield put({ type: 'getMemTypes' });
      yield put({ type: 'getMemLevels' });
    },

    *saveMember({ payload }, { call }) {
      const { params, success } = payload;
      const { code, message: result } = yield call(interact('saveMember'), params);

      if (code === statusOk) {
        success(result.Member);
      } else {
        throw new Error(result);
      }
    },

    *updateMember({ payload }, { call }) {
      const { params } = payload;
      const { code, message: result } = yield call(interact('updateMember', params.MemId), params);

      if (code === statusOk) {
        message.success('更新成功');
      } else {
        throw new Error(result);
      }
    },
  },
  subscriptions: {
    setup({ history, dispatch }) {
      // Subscribe history(url) change, trigger `load` action if pathname is `/`
      return history.listen((location) => {
        let match = pathToRegexp('/member/list').exec(location.pathname);
        if (match) {
          dispatch({ type: 'loadList', payload: searchParams(location.search) });
        }

        match = pathToRegexp('/member/edit/:memId?').exec(location.pathname);
        if (match) {
          dispatch({ type: 'loadEdit', payload: match[1] });
        }
      });
    },
  },
});
