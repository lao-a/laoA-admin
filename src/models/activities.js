import modelExtend from 'dva-model-extend';
import { routerRedux } from 'dva/router';
import pathToRegexp from 'path-to-regexp';
import { message } from 'antd';
import base from './base';
import { statusOk } from '../utils/httpcode';
import { searchParams } from '../utils/utils';
import { interact } from '../config/api';

const saveActivity = interact('saveActivity');

const PAGE_LIST = 'index_list';

export default modelExtend(base, {
  namespace: 'activities',
  state: {
    types: [],
  },
  effects: {
    *saveActivity({ payload }, { call }) {
      const { params, success } = payload;
      const { code, message: result } = yield call(saveActivity, params);

      if (code === statusOk) {
        success(result.Activity);
      } else {
        throw new Error(result);
      }
    },

    *updateActivity({ payload }, { call }) {
      const { params } = payload;
      const { code, message: result } = yield call(interact('updateActivity', params.ActivityId), params);

      if (code === statusOk) {
        message.success('更新成功');
      } else {
        throw new Error(result);
      }
    },

    *loadTypes(_, { call, put }) {
      const { code, message: result } = yield call(interact('getActivityTypes'));
      if (code === statusOk) {
        yield put({ type: 'syncState', payload: { types: result.ActivityTypes } });
      } else {
        throw new Error(result);
      }
    },

    *loadList({ payload }, { call, put }) {
      // payload 为url search
      // 暂时只支持 page
      const page = payload.page || 0;
      const { code, message: result } = yield call(interact('getActivityList'), { page });

      if (code === statusOk) {
        yield put({ type: 'syncState', payload: { list: result.Activities } });
        yield put({ type: 'syncPageNavi', payload: { id: PAGE_LIST, current: page } });
      } else {
        throw new Error(result);
      }

      // 获取分类
      yield put({ type: 'loadTypes' });
    },

    *loadEdit({ payload }, { call, put }) {
      if (payload) {
        const { code, message: result } = yield call(interact('getActivity', [payload]));

        if (code === statusOk) {
          if (!result.Activity.ActivityId) {
            throw new Error('没有找到对应的活动');
          }

          yield put({ type: 'syncState', payload: { detail: result.Activity } });
        } else {
          throw new Error(result);
        }
      }

      // 获取分类
      yield put({ type: 'loadTypes' });
    },
    reducers: {
    },
  },
  subscriptions: {
    setup({ history, dispatch }) {
      // Subscribe history(url) change, trigger `load` action if pathname is `/`
      return history.listen((location) => {
        let match = pathToRegexp('/activity/list').exec(location.pathname);
        if (match) {
          dispatch({ type: 'loadList', payload: searchParams(location.search) });
        }

        match = pathToRegexp('/activity/edit/:actId?').exec(location.pathname);
        if (match) {
          dispatch({ type: 'loadEdit', payload: match[1] });
        }
      });
    },
  },
});
