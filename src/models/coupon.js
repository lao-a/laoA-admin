import modelExtend from 'dva-model-extend';
import { routerRedux } from 'dva/router';
import pathToRegexp from 'path-to-regexp';
import { message } from 'antd';
import base from './base';
import { statusOk } from '../utils/httpcode';
import { searchParams } from '../utils/utils';
import { interact } from '../config/api';

const PAGE_LIST = 'coupon_list';

export default modelExtend(base, {
  namespace: 'coupon',
  state: {
    types: [],
  },
  effects: {
  },
  reducers: {
  },
  subscriptions: {
    setup({ history, dispatch }) {
      // Subscribe history(url) change, trigger `load` action if pathname is `/`
      // return history.listen((location) => {
      //   let match = pathToRegexp('/activity/list').exec(location.pathname);
      //   if (match) {
      //     dispatch({ type: 'loadList', payload: searchParams(location.search) });
      //   }

      //   match = pathToRegexp('/activity/edit/:actId?').exec(location.pathname);
      //   if (match) {
      //     dispatch({ type: 'loadEdit', payload: match[1] });
      //   }
      // });
    },
  },
});
