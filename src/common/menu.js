import { isUrl } from '../utils/utils';

const menuData = [
  {
    name: '面板',
    icon: 'dashboard',
    path: 'dashboard',
  },
  {
    name: '活动',
    icon: 'user',
    path: 'activity/list',
    // children: [
    //   {
    //     name: '详情',
    //     hideInMenu: true,
    //     path: 'edit',
    //   },
    // ],
  },
  {
    name: '会员',
    icon: 'user',
    path: 'member/list',
    // children: [
    //   {
    //     name: '详情',
    //     hideInMenu: true,
    //     path: 'edit',
    //   },
    // ],
  },
  {
    name: '设置',
    icon: 'setting',
    path: '/setting',
    children: [
      {
        name: '优惠券',
        path: 'coupon',
      }
    ],
  },
];

function formatter(data, parentPath = '/', parentAuthority) {
  return data.map((item) => {
    let { path } = item;
    if (!isUrl(path)) {
      path = parentPath + item.path;
    }
    const result = {
      ...item,
      path,
      authority: item.authority || parentAuthority,
    };
    if (item.children) {
      result.children = formatter(item.children, `${parentPath}${item.path}/`, item.authority);
    }
    return result;
  });
}

export const getMenuData = () => formatter(menuData);
