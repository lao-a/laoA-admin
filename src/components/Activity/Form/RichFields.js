import React, { PureComponent } from 'react';
import { Tabs } from 'antd';
import RichEditor from '../../RichEditor';

const TabPane = Tabs.TabPane;

class RichFields extends PureComponent {
  handleImageUpload = (file) => {
    if (!this.props.onImageUpload) {
      return Promise.reject('图片上传失败, 无上传文件接口');
    }

    return this.props.onImageUpload(file);
  }

  render() {
    const { form, data } = this.props;

    return (
      <Tabs defaultActiveKey="1" style={{ width: '100%' }}>
        <TabPane tab="活动特色" key="1">
        {
          form.getFieldDecorator('Special', {
            initialValue: data.Special,
            rules: [{
              required: true, message: '请填写活动特色',
            }],
          })(<RichEditor onUploadImage={this.handleImageUpload} />)
        }
        </TabPane>
        <TabPane tab="基本情况" key="2">
        {
          form.getFieldDecorator('BasicInfo', {
            initialValue: data.BasicInfo,
            rules: [{
              required: true, message: '请填写活动基本情况',
            }],
          })(<RichEditor onUploadImage={this.handleImageUpload} />)
        }
        </TabPane>
        <TabPane tab="行程安排" key="3">
        {
          form.getFieldDecorator('TravelPlan', {
            initialValue: data.TravelPlan,
            rules: [{
              required: true, message: '请填写活动行程安排',
            }],
          })(<RichEditor onUploadImage={this.handleImageUpload} />)
        }
        </TabPane>
        <TabPane tab="费用详情" key="4">
        {
          form.getFieldDecorator('AboutFee', {
            initialValue: data.AboutFee,
            rules: [{
              required: true, message: '请填写活动费用详情',
            }],
          })(<RichEditor onUploadImage={this.handleImageUpload} />)
        }
        </TabPane>
        <TabPane tab="报名方式" key="5">
        {
          form.getFieldDecorator('Contact', {
            initialValue: data.Contact,
            rules: [{
              required: true, message: '请填写活动报名方式',
            }],
          })(<RichEditor onUploadImage={this.handleImageUpload} />)
        }
        </TabPane>
      </Tabs>
    );
  }
}

export default RichFields;
