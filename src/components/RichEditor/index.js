import React from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const defaultModules = {
  toolbar: [
    [{ header: '1' }, { header: '2' }],
    [{ size: [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    ['link', 'image', 'video'],
    ['clean'],
  ],
  clipboard: {
    // toggle to add extra line breaks when pasting HTML:
    matchVisual: false,
  },
};

const defaultFormats = [
  'header',
  'size',
  'bold', 'italic', 'underline', 'strike', 'blockquote',
  'link', 'image', 'video',
];

class RichEditor extends React.Component {
  constructor(props) {
    super(props);
    this.quill = null; // Quill instance
    this.reactQuill = null; // ReactQuill component
    this.state = {
      value: this.props.value || '',
    };

    // onUploadImage 必须是 Promise
    if (this.props.onUploadImage) {
      this.image = window.document.createElement('input');
      this.image.type = 'file';
      this.image.accept = 'image/*';
      this.image.onchange = this.hangeImageChange;
    }
  }

  componentDidMount() {
    this.attachQuillRefs();
  }

  componentWillReceiveProps(nextprops) {
    if (nextprops.value && nextprops.value !== this.state.value) {
      this.setState({ value: nextprops.value });
    }
  }

  componentDidUpdate() {
    this.attachQuillRefs();
  }

  attachQuillRefs = () => {
    if (typeof this.reactQuill.getEditor !== 'function') return;
    this.quill = this.reactQuill.getEditor();

    // onUploadImage 必须是 Promise
    if (this.props.onUploadImage) {
      this.quill.getModule('toolbar').addHandler('image', () => this.image.click());
    }
  }

  handleChange = (html) => {
    if (this.quill) {
      this.setState(
        { value: html },
        () => {
          if (this.props.onChange) {
            this.props.onChange(html);
          }
        },
      );
    }
  }

  hangeImageChange = (e) => {
    const file = e.target.files[0];

    this.props.onUploadImage(file).then((url) => {
      const inx = this.quill.getSelection().index;
      this.quill.insertEmbed(inx, 'image', url);
      this.quill.setSelection(inx + 1, 0);
    });
  }

  render() {
    const ToolBar = this.props.toolBar || undefined;

    // 生成纯净的 React-Qill props
    const props = {
      ...this.props,
      modules: this.props.modules || (this.props.toolBar ? undefined : defaultModules),
      formats: this.props.formats || defaultFormats,
      onChange: undefined, // 不传递给 React-Qill
      className: undefined,
      toolBar: undefined,
    };

    return (
      <div className={this.props.className}>
        {
          ToolBar && <ToolBar />
        }
        <ReactQuill
          {...props}
          value={this.state.value}
          onChange={this.handleChange}
          ref={(el) => { this.reactQuill = el; }}
        />
      </div>
    );
  }
}

export default RichEditor;
