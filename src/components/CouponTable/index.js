import React, { PureComponent } from 'react';
import moment from 'moment';
import { Table, Alert, Badge } from 'antd';
import { Link } from 'dva/router';
import Ellipsis from '../Ellipsis';
import styles from './index.less';

const statusMap = ['default', 'processing', 'success', 'error'];
class CouponTable extends PureComponent {
  state = {
    selectedRowKeys: [],
  };

  componentWillReceiveProps(nextProps) {
    // clean state
    if (nextProps.selectedRows.length === 0) {
      this.setState({
        selectedRowKeys: [],
      });
    }
  }

  handleRowSelectChange = (selectedRowKeys, selectedRows) => {
    if (this.props.onSelectRow) {
      this.props.onSelectRow(selectedRows);
    }

    this.setState({ selectedRowKeys });
  }

  handleTableChange = (pagination, filters, sorter) => {
    this.props.onChange(pagination, filters, sorter);
  }

  cleanSelectedKeys = () => {
    this.handleRowSelectChange([], []);
  }

  render() {
    const { selectedRowKeys } = this.state;
    const { data: { list, pagination }, loading } = this.props;

    const columns = [
      {
        title: '卡券名称',
        dataIndex: 'Name',
      },
      {
        title: '卡券描述',
        dataIndex: 'Desc',
        render: val => (<Ellipsis tooltip length={20}>{val}</Ellipsis>),
      },
      {
        title: '金额',
        dataIndex: 'Charge',
      },
      {
        title: '有效期',
        render: (t, r) => {
          const expireType = r.ExpireType;
          if (!expireType) return (<Badge count="未设置" />);

          if (expireType === 'by-range') {
            // 按区间
            return (
              <span>
                `${moment(r.ExpireStart).format('YYYY-MM-DD HH:mm')} - ${moment(r.ExpireEnd).format('YYYY-MM-DD HH:mm')}`
                {
                  moment().isAfter(moment(r.ExpireEnd)) &&
                  (<Badge count='已过期' />)
                }
              </span>
            );
          } else {
            // 按时间
            return (<Badge count={`${r.ExpireTo} 天`} style={{ backgroundColor: '#1890ff' }} />)
          }
        },
      },
      {
        title: '创建时间',
        dataIndex: 'CreateDate',
        sorter: true,
        render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm')}</span>,
      },
      {
        title: '操作',
        render: (t, r) => (
          <p>
            <Link to={`/setting/coupon/edit/${r.CoupinId}`}>详情</Link>
            <span className={styles.splitLine} />
            <a href="">禁用</a>
          </p>
        ),
      },
    ];

    const paginationProps = {
      showSizeChanger: true,
      showQuickJumper: true,
      ...pagination,
    };

    const rowSelection = {
      selectedRowKeys,
      onChange: this.handleRowSelectChange,
      getCheckboxProps: record => ({
        disabled: record.disabled,
      }),
    };

    return (
      <div className={styles.CouponTable}>
        <Table
          loading={loading}
          rowKey={record => record.MemId}
          rowSelection={rowSelection}
          dataSource={list}
          columns={columns}
          pagination={paginationProps}
          onChange={this.handleTableChange}
        />
      </div>
    );
  }
}

export default CouponTable;
