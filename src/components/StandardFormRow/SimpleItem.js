import React from 'react';
import { Form } from 'antd';

export default (props) => {
  return (
    <Form.Item
      labelCol={{
        xs: { span: 24 },
        sm: { span: 6 },
      }}
      wrapperCol={{
        xs: { span: 24 },
        sm: { span: 14 },
      }}
      label={props.label}
    >
      {props.children}
    </Form.Item>
  );
};
